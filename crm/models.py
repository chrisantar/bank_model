from django.db import models
import random


def get_account_id():
    return random.randint(1000, 10000)


class User(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.CharField(max_length=75)

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)

    def get_user_balance(self):
        user_balance = 0
        for account in self.get_accounts():
            user_balance += account.get_account_balance()
        return user_balance

    def get_accounts(self):
        return Account.objects.filter(user=self)


class Account(models.Model):
    account_name = models.CharField(max_length=50)
    account_number = models.IntegerField(default=get_account_id)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.account_name

    # def get_user_name(self):
        # return User.objects.get(id=self.user).first_name

    def get_account_balance(self):
        account_balance = 0
        for transaction in self.get_transactions():
            account_balance += transaction.transaction_amt
        return account_balance

    def get_transactions(self):
        return Transaction.objects.filter(account=self)


class Transaction(models.Model):
    pub_date = models.DateTimeField('date published')
    transaction_amt = models.IntegerField(default=0)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)

    def __str__(self):
        return "{} {}".format(self.account, self.pub_date)
