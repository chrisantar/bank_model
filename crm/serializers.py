from rest_framework import generics
from rest_framework import serializers
from . import models


class UserSerializer(serializers.ModelSerializer):
    user_balance = serializers.IntegerField(source='get_user_balance')

    class Meta:
        model = models.User
        fields = ('id', 'first_name', 'last_name', 'email', 'user_balance')


class AccountSerializer(serializers.ModelSerializer):
    account_balance = serializers.IntegerField(source='get_account_balance')
   # parent_user_name = serializers.CharField(max_length=50, source='get_user_name')

    class Meta:
        model = models.Account
        fields = ('id', 'account_name', 'account_number',
                  'account_balance', 'user', 'account_balance')


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Transaction
        fields = ('id', 'pub_date', 'transaction_amt', 'account')
