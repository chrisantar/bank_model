
from django.shortcuts import render
from .models import User, Account, Transaction
from django.http import HttpResponse
from rest_framework import generics
from . import serializers


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer


class AccountList(generics.ListAPIView):
    queryset = Account.objects.all()
    serializer_class = serializers.AccountSerializer


class TransactionList(generics.ListAPIView):
    queryset = Transaction.objects.all()
    serializer_class = serializers.TransactionSerializer


class UserRList(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer


class AccountRList(generics.RetrieveAPIView):
    queryset = Account.objects.all()
    serializer_class = serializers.AccountSerializer


class TransactionRList(generics.RetrieveAPIView):
    queryset = Transaction.objects.all()
    serializer_class = serializers.TransactionSerializer


class UserAccountList(generics.ListAPIView):
    def get_queryset(self):
        return Account.objects.filter(user=self.kwargs.get('pk'))
    serializer_class = serializers.AccountSerializer


class AccountTransactionList(generics.ListAPIView):
    def get_queryset(self):
        return Transaction.objects.filter(account=self.kwargs.get('pk'))
    serializer_class = serializers.TransactionSerializer


# def APIList(request):
    # user_list = User.objects.order_by('first_name')[0:]
    # account_list = Account.objects.order_by('account_name')[0:]
    # transaction_list = Transaction.objects.order_by('pub_date')[0:]
    # context = {'user_list': user_list,
    #           'account_list': account_list,
    #          'transaction_list': transaction_list
    #          }
    # return render(request, 'crm/APIList.html', context)


# def APIDetail(request):
    # return HttpResponse("Test")
