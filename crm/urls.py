from django.urls import path
from . import views as crm_views
from django.conf.urls import url


urlpatterns = [
    path('user', crm_views.UserList.as_view(), name='User-List'),
    path('account', crm_views.AccountList.as_view(), name='Account-List'),
    path('transaction', crm_views.TransactionList.as_view(),
         name='Transaction-List'),
    path('user/<int:pk>', crm_views.UserRList.as_view(), name="User-R-List"),
    path('account/<int:pk>', crm_views.AccountRList.as_view(), name="Account-R-List"),
    path('transaction/<int:pk>', crm_views.TransactionRList.as_view(),
         name="Transaction-R-List"),
    path('user/<int:pk>/account', crm_views.UserAccountList.as_view(),
         name="User-Account-List"),
    path('account/<int:pk>/transaction',
         crm_views.AccountTransactionList.as_view(), name="ACcount-Transaction-List"),
]
